package com.wysa.sleephealth.Services;

import java.util.ArrayList;
import java.util.List;

import com.wysa.sleephealth.Bean.QuestionBean;
import com.wysa.sleephealth.Exception.DBEmptyResponseException;
import com.wysa.sleephealth.Repository.SleepRepository;
import com.wysa.sleephealth.RepositoryImplementaions.QuestionInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService  {

    @Autowired
    QuestionInterface questionInterface;

    @Override
    public List<QuestionBean> getAllQuestionAnswers() throws DBEmptyResponseException{
        // TODO Auto-generated method stub

        ArrayList<QuestionBean> questions = new ArrayList<QuestionBean>();

        if(questions == null || questions.size()==0){
            throw new DBEmptyResponseException();
        }
        
        return questions;
    }

    @Override
    public QuestionBean getQuestionByNumber(int questionNumber)  {
        // TODO Auto-generated method stub

        return questionInterface.getQuestionByNumber(questionNumber);

    }
    
}
