package com.wysa.sleephealth.Services;

import com.wysa.sleephealth.Bean.Response;
import com.wysa.sleephealth.RepositoryImplementaions.UserRepositoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService{


    @Autowired
    UserRepositoryService userRepository;

    @Override
    public void addUserResponse(Response response) {
        // TODO Auto-generated method stub
        userRepository.addUserResponse(response);
        
    }
    
}
