package com.wysa.sleephealth.Services;

import java.util.List;

import com.wysa.sleephealth.Bean.QuestionBean;
import com.wysa.sleephealth.Exception.DBEmptyResponseException;

public interface QuestionService {
    
    List<QuestionBean> getAllQuestionAnswers() throws DBEmptyResponseException;

    QuestionBean getQuestionByNumber(int id);    
}
