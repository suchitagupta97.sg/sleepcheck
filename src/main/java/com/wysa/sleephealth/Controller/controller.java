package com.wysa.sleephealth.Controller;

import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.wysa.sleephealth.Bean.GetRequest;
import com.wysa.sleephealth.Bean.QuestionBean;
import com.wysa.sleephealth.Bean.Response;
import com.wysa.sleephealth.Exception.DBEmptyResponseException;
import com.wysa.sleephealth.Repository.UserResponseRepository;
import com.wysa.sleephealth.RepositoryImplementaions.UserRepositoryService;
import com.wysa.sleephealth.Services.QuestionService;
import com.wysa.sleephealth.Services.UserService;

import org.modelmapper.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(controller.HOME)
public class controller {

    static final String HOME = "/home";
    static final String QUESTION = "/question";
    static final String ANSWER = "/answer";

    @Autowired
    QuestionService questionService;

    @Autowired
    UserService userService;


    @Autowired
    UserResponseRepository users;

    @GetMapping(QUESTION)
    ResponseEntity<Response> questionRequest(GetRequest request){

        try{
         
            QuestionBean question = questionService.getQuestionByNumber(request.getQuestionId());

             if(question == null){
                throw new DBEmptyResponseException();
            }

            Response r = new Response();
            r.setUserId(request.getId());
            r.setUserName(request.getUserName());
            r.setUserResponse(question);


            return ResponseEntity.ok().body(r);


        }  catch(DBEmptyResponseException ex){

            System.out.println("DB inresponsive-------"+ ex.getMessage());
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.valueOf(400)).body(null);

        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.valueOf(400)).body(null);

        }

    }


    @PostMapping(ANSWER)
    ResponseEntity answerRequest(@RequestBody Response response){

        try{

            int quesId = response.getUserResponse().getId();
            int sleepHours = 0;
            String sleepEfficiency = "Your sleeping hours efficiency is ";

            userService.addUserResponse(response);

            if(quesId == 4){
                sleepHours = Integer.parseInt(response.getUserResponse().getAnswers().get(0));
                    sleepEfficiency +=(sleepHours * 10)+" %";

                    if(sleepHours  < 8)
                        sleepEfficiency += "\n We'll get this to up to 80%";
                    else
                        sleepEfficiency += " Great job !!";

                sleepEfficiency += "\n A higher sleep efficiency score means a more refreshing and energizing " 
                                + "sleep, which can help you move into your day with a sense of lightness and ease";
                return ResponseEntity.ok().body(sleepEfficiency);    
            }

            return ResponseEntity.ok().body("That's great, lets move ahead");

        } catch(Exception ex){

            return ResponseEntity.status(HttpStatus.valueOf(400)).body(ex.getCause()+ "error  "+ex.getMessage());
            }
        }


    @GetMapping("/user")
    ResponseEntity users(){

        try{

          return ResponseEntity.ok().body(users.findAll());

        } catch(Exception ex){

            return ResponseEntity.status(HttpStatus.valueOf(400)).body(null);
        }

    }

    
}
