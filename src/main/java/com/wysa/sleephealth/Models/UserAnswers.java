package com.wysa.sleephealth.Models;

import java.util.ArrayList;
import com.wysa.sleephealth.Bean.QuestionBean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class UserAnswers {

    @Id
    int userId;

    String userName;

    ArrayList<QuestionEntity> quesAns = new ArrayList<QuestionEntity>();

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ArrayList<QuestionEntity> getQuesAns() {
        return quesAns;
    }

    public void setQuesAns(ArrayList<QuestionEntity> quesAns) {
        this.quesAns = quesAns;
    }

    @Override
    public String toString() {
        return "UserAnswers [ quesAns=" + quesAns + ", userId=" + userId + ", userName=" + userName + "]\n";
    }

    
}
