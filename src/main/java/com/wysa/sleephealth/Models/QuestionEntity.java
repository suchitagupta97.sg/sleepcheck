package com.wysa.sleephealth.Models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class QuestionEntity {

    @Id
    int id;

    String question;
    
    ArrayList<String> answer = new ArrayList<String>();

    public QuestionEntity() {

    }

    public QuestionEntity(int id,String question, ArrayList<String> answer){
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ArrayList<String> getAnswer() {
        return answer;
    }

    public void setAnswer(ArrayList<String> answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "QuestionEntity [ id=" + id +  ", question=" + question + " answer=" + answer +"]";
    }

    
    
}
