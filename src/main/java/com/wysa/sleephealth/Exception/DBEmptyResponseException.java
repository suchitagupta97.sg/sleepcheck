package com.wysa.sleephealth.Exception;

public class DBEmptyResponseException extends Exception{
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DBEmptyResponseException(){
        super("DB is not responding...");
    }
}
