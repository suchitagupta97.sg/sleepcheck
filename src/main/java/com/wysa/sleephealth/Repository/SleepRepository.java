package com.wysa.sleephealth.Repository;

import java.util.Optional;

import com.wysa.sleephealth.Models.QuestionEntity;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface SleepRepository extends MongoRepository<QuestionEntity,Integer>{

    @Query("{id:?0}") 
    QuestionEntity getQuestionByNumber(int questionId);
    
}
