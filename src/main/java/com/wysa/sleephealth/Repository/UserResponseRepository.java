package com.wysa.sleephealth.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.wysa.sleephealth.Models.QuestionEntity;
import com.wysa.sleephealth.Models.UserAnswers;

public interface UserResponseRepository extends MongoRepository<UserAnswers,Integer>{

}