package com.wysa.sleephealth;

import com.wysa.sleephealth.Repository.SleepRepository;
import com.wysa.sleephealth.Repository.UserResponseRepository;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(exclude = MongoAutoConfiguration.class)
//@EnableMongoRepositories
public class SleepHealthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SleepHealthApplication.class, args);
	}

	@Bean
	ModelMapper getModelMapper(){
		return new ModelMapper();
	}
}
