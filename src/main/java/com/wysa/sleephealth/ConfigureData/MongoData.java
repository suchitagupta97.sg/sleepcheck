package com.wysa.sleephealth.ConfigureData;

import java.util.ArrayList;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.wysa.sleephealth.Models.QuestionEntity;
import com.wysa.sleephealth.Models.UserAnswers;
import com.wysa.sleephealth.Repository.SleepRepository;
import com.wysa.sleephealth.Repository.UserResponseRepository;
import com.wysa.sleephealth.RepositoryImplementaions.UserRepositoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Component;

@Configuration
@EnableMongoRepositories(basePackageClasses = SleepRepository.class)
public class MongoData implements CommandLineRunner{

    @Autowired
    SleepRepository sleepRepository;

    @Autowired
    UserResponseRepository userResponseRepository;

    public @Bean MongoClient mongoClient() {
        return MongoClients.create("mongodb://localhost:27017");
    }
  
    public @Bean MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), "sleepDatabase");
    }

    @Override
    public void run(String... args) throws Exception {
        // TODO Auto-generated method stub

        if(sleepRepository.findAll().size()!=0){

            for(QuestionEntity qe : sleepRepository.findAll()){
                System.out.println(qe.toString());
            }

            for(UserAnswers u: userResponseRepository.findAll()){
                System.out.println(u.toString());
            }
            System.out.print("Data Dumped");   
            userResponseRepository.deleteAll();
            return;
        }
            
        ArrayList<String> answers = new ArrayList<String>();
        answers.add("I would go to sleep easily");
        answers.add("I would sleep through the night");
        answers.add("I would wake up on time, refreshed");
        //adding question number one with its option
        sleepRepository.save(new QuestionEntity(1,"Lets say in few weeks, you're sleeping well.What would change",
                        answers));

        System.out.println("done dumping...."+ sleepRepository.findAll().size());

        //adding question number two
        answers.clear();
        answers.add("Less than 2 weeks");
        answers.add("2 to 8 weeks");
        answers.add("More than 8 weeks");

        sleepRepository.save(new QuestionEntity(2,"That's a great goal. How long have you been strugglung with sleep?"
                        ,answers));
        
        System.out.println("done dumping...."+ sleepRepository.findAll().size());

        //adding question number three
        sleepRepository.save(new QuestionEntity(3,"What time do you go to bed for sleep"
                        ,null));
        
        //adding question number four
        sleepRepository.save(new QuestionEntity(4,"How many hours sleep do you get in a typical night",null));  
        System.out.println("done dumping...."+ sleepRepository.findAll().size());   
        
       

        
    }
    
    
}
