package com.wysa.sleephealth.RepositoryImplementaions;

import java.util.List;

import com.wysa.sleephealth.Bean.QuestionBean;

public interface QuestionInterface {

    List<QuestionBean> getAllQuestionAnswers();

    QuestionBean getQuestionByNumber(int id);
}