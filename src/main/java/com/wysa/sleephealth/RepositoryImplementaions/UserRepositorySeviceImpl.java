package com.wysa.sleephealth.RepositoryImplementaions;

import java.util.ArrayList;
import java.util.Optional;

import javax.swing.event.AncestorEvent;

import com.wysa.sleephealth.Bean.Response;
import com.wysa.sleephealth.Models.QuestionEntity;
import com.wysa.sleephealth.Models.UserAnswers;
import com.wysa.sleephealth.Repository.UserResponseRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class UserRepositorySeviceImpl implements UserRepositoryService{

    @Autowired
    UserResponseRepository userResponseRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void addUserResponse(Response userAnswers) {
        // TODO Auto-generated method stub

        QuestionEntity q = modelMapper.map(userAnswers.getUserResponse(), QuestionEntity.class);


        UserAnswers uAnswers = new UserAnswers();
        
        uAnswers.setUserId(userAnswers.getUserId());
        uAnswers.setUserName(userAnswers.getUserName());
        ArrayList<QuestionEntity> al = new ArrayList<QuestionEntity>();
        al.add(q);
        ArrayList<String> ans = new ArrayList<String>();
        ans.add(userAnswers.getUserResponse().getAnswers().get(0));
        al.get(0).setAnswer(ans);
        uAnswers.setQuesAns(al);
       
        if(userResponseRepository.findById(userAnswers.getUserId()).isPresent() == false){
            userResponseRepository.save(uAnswers);
            return;
        } else {
            
           UserAnswers answersEntity =  userResponseRepository.findById(userAnswers.getUserId()).get();
           ArrayList<QuestionEntity> questionEntities = answersEntity.getQuesAns();

           for(QuestionEntity qe : questionEntities){
             if(qe.getId() == userAnswers.getUserResponse().getId()){
                 qe.getAnswer().add(userAnswers.getUserResponse().getAnswers().get(0));
                 userResponseRepository.save(uAnswers);
                 return;
             }
           }

           answersEntity.getQuesAns().add(q);
           userResponseRepository.save(answersEntity);
        }
    
    }
    
}
