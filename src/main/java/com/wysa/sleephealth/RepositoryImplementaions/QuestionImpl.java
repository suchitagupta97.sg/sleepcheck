package com.wysa.sleephealth.RepositoryImplementaions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.wysa.sleephealth.Bean.QuestionBean;
import com.wysa.sleephealth.Models.QuestionEntity;
import com.wysa.sleephealth.Repository.SleepRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class QuestionImpl implements QuestionInterface{

    @Autowired
    SleepRepository sleepRepository;

    @Autowired
    ModelMapper modelMapper;
    
    @Override
    public List<QuestionBean> getAllQuestionAnswers() {
        // TODO Auto-generated method stub

        List<QuestionEntity> questions = sleepRepository.findAll();

        if(questions == null || questions.size()==0)
            return Collections.emptyList();
        
        List<QuestionBean> questionBeans = new ArrayList<QuestionBean>();

        for(QuestionEntity qe : questions){
            questionBeans.add(modelMapper.map(qe,QuestionBean.class));
        }

        return questionBeans;
    }

    @Override
    public QuestionBean getQuestionByNumber(int id) {
        // TODO Auto-generated method stub

        QuestionEntity question = sleepRepository.getQuestionByNumber(id);
        if(question != null) {
            
            QuestionBean q =modelMapper.map(question,QuestionBean.class);
            q.setAnswers(question.getAnswer());
            
            return q;//modelMapper.map(question,QuestionBean.class);
        }
        
        return null;
    }
    
}



