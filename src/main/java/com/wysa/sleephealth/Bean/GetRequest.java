package com.wysa.sleephealth.Bean;

import lombok.NonNull;

public class GetRequest {

    @NonNull
    int id;
    @NonNull
    String userName;
    @NonNull
    int questionId;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public int getQuestionId() {
        return questionId;
    }
    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
}
