package com.wysa.sleephealth.Bean;

import java.util.ArrayList;


public class QuestionBean {
    
    int id;
    String question;
    ArrayList<String> answers = new ArrayList<String>();

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public ArrayList<String> getAnswers() {
        return answers;
    }
    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }
}
