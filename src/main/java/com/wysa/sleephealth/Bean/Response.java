package com.wysa.sleephealth.Bean;

public class Response {

    private int userId;

    private String userName;

    private QuestionBean userResponse;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public QuestionBean getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(QuestionBean userResponse) {
        this.userResponse = userResponse;
    }
   
}
